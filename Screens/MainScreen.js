import React, { useState } from 'react'
import { View, StyleSheet, FlatList } from 'react-native'
import Contact from '../Components/Contact';
import Search from '../Components/Search';
import { data } from '../Data'

let contactList = [...data]

export default function MainScreen({ navigation }) {

  const [contacts, setContacts] = useState(contactList);

  const openContactHandler = (contact, onRemove) => {
    console.log("openContactHandler -> onRemove", onRemove)
    navigation.navigate('Contact', { contact, onRemove })
  }

  const onRemoveContactHandler = id => {
    console.log("MainScreen -> id", id)
    const newContactList = contactList.filter(contact => id !== contact.id)
    contactList = newContactList
    setContacts(contactList)
    navigation.navigate('Main')
  }

  const onSearchHandler = string => {
    const newContactList = contactList.filter(contact => {
      for (const key in contact) {
        if (key !== 'id' && key !== 'avatar') {
          if (contact[key].toLowerCase().indexOf(string.toLowerCase()) !== -1) {
            return true
          }
        }
      }
      return false
    })
    setContacts(newContactList)
  }

  return (
    <View style={styles.container}>
      <Search onChange={onSearchHandler} />
      <FlatList
        style={styles.contactList}
        keyExtractor={(contact) => contact.id}
        data={contacts}
        renderItem={({ item }) => <Contact contact={item} onOpen={openContactHandler} onRemove={onRemoveContactHandler} />}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingBottom: 70
  },
  contactList: {
    marginTop: 20,
    marginHorizontal: 20,
  },
});