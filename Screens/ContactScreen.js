import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Field from '../Components/Field'
import ButtonsBlock from '../Components/ButtonsBlock'

const ContactScreen = (props) => {
  const contact = props.route.params.contact
  const onRemove = props.route.params.onRemove
  // console.log("ContactScreen -> contact", contact)
  console.log("ContactScreen -> contact", contact.avatar)
  return (
    <View style={styles.component}>
      <Image style={styles.avatar} source={contact.avatar ? { uri: contact.avatar } : require('../img/avatar.png')} />
      <Field title='Name' value={`${contact.name} ${contact.secondName}`}></Field>
      <Field title='Phone number' value={contact.phone}></Field>
      <ButtonsBlock phoneNumber={contact.phone} onRemove={onRemove} id={contact.id} />
    </View>
  )
}

const styles = StyleSheet.create({
  component: {
    alignItems: 'flex-start',
    padding: 20
  },

  avatar: {
    width: 100,
    height: 100,
    marginTop: 30,
    alignSelf: 'center'
  }
})

export default ContactScreen

