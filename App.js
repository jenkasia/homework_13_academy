import React, {useState} from 'react';
import {StyleSheet, View, Text, FlatList} from 'react-native';
import Search from './Components/Search';
import Contact from './Components/Contact';
// import {AppNavigation} from './navigation/AppNavigatioin';
// import {AppNavigation} from './navigation/AppNavigatioin';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import MainScreen from './Screens/MainScreen';
import ContactScreen from './Screens/ContactScreen';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">
        <Stack.Screen name="Main" component={MainScreen} />
        <Stack.Screen name="Contact" component={ContactScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

// export default function App() {
// return <AppNavigation />;
// }

//   const [contacts, setContacts] = useState([
//     {
//       id: '1',
//       name: 'Ilon',
//       secondName: 'Mask',
//       phone: '+380662222222',
//       mail: 'ilon.mask@gmail.com',
//     },
//     {
//       id: '2',
//       name: 'Ilon',
//       secondName: 'Mask',
//       phone: '+380662222222',
//       mail: 'ilon.mask@gmail.com',
//     },
//     {
//       id: '3',
//       name: 'Ilon',
//       secondName: 'Mask',
//       phone: '+380662222222',
//       mail: 'ilon.mask@gmail.com',
//     },
//     {
//       id: '4',
//       name: 'Ilon',
//       secondName: 'Mask',
//       phone: '+380662222222',
//       mail: 'ilon.mask@gmail.com',
//     },
//     {
//       id: '5',
//       name: 'Ilon',
//       secondName: 'Mask',
//       phone: '+380662222222',
//       mail: 'ilon.mask@gmail.com',
//     },
//     {
//       id: '6',
//       name: 'Ilon',
//       secondName: 'Mask',
//       phone: '+380662222222',
//       mail: 'ilon.mask@gmail.com',
//     },
//     {
//       id: '7',
//       name: 'Ilon',
//       secondName: 'Mask',
//       phone: '+380662222222',
//       mail: 'ilon.mask@gmail.com',
//     },
//     {
//       id: '8',
//       name: 'Ilon',
//       secondName: 'Mask',
//       phone: '+380662222222',
//       mail: 'ilon.mask@gmail.com',
//     },
//     {
//       id: '9',
//       name: 'Ilon',
//       secondName: 'Mask',
//       phone: '+380662222222',
//       mail: 'ilon.mask@gmail.com',
//     },
//     {
//       id: '10',
//       name: 'Ilon',
//       secondName: 'Mask',
//       phone: '+380662222222',
//       mail: 'ilon.mask@gmail.com',
//     },
//     {
//       id: '11',
//       name: 'Ilon',
//       secondName: 'Mask',
//       phone: '+380662222222',
//       mail: 'ilon.mask@gmail.com',
//     },
//     {
//       id: '12',
//       name: 'Ilon',
//       secondName: 'Mask',
//       phone: '+380662222222',
//       mail: 'ilon.mask@gmail.com',
//     },
//   ]);

//   return (
//     <View style={styles.container}>
//       <Search />
//       <FlatList
//         style={styles.contactList}
//         keyExtractor={(contact) => contact.id}
//         data={contacts}
//         renderItem={({item}) => <Contact contact={item} />}
//       />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {},
//   contactList: {
//     marginTop: 20,
//     marginHorizontal: 20,
//   },
// });
