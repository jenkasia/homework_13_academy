import React, { useState } from 'react';
import { TextInput, View, Image, StyleSheet } from 'react-native';
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

const Search = ({ onChange }) => {
  const [value, setValue] = useState();
  return (
    <View style={styles.SectionStyle}>
      <View>
        <FontAwesomeIcon style={styles.icon} icon={faSearch} />
      </View>
      <TextInput
        style={styles.search}
        value={value}
        placeholder="Search"
        onChangeText={(text) => {
          on(text)
          setValue(text)
        }}
      />
    </View>)
}


const styles = StyleSheet.create({
  search: {
    flex: 1,
  },
  icon: {
    marginRight: 4,
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#000',
    height: 40,
    borderRadius: 20,
    marginHorizontal: 20,
    marginTop: 20,
    paddingLeft: 10,
  },

  ImageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
    alignItems: 'center',
  }
})
export default Search;
