import React from 'react'
import { View, StyleSheet, Text } from 'react-native'

const Field = ({ title, value }) => {
  return (
    <View style={styles.component}>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.value}>{value}</Text>

    </View>
  )
}

const styles = StyleSheet.create({

  component: {
    marginTop: 50,
    width: '100%',
    fontSize: 20,
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#aaaaaa',
    paddingBottom: 5,
  },
  title: {
    fontSize: 14
  },
  value: {
    fontSize: 20,
    marginTop: 10,
  }
})

export default Field
