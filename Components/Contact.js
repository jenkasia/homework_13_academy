import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import { faIdCardAlt, faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import Swipeout from 'react-native-swipeout';


const Contact = ({ contact, onOpen, onRemove }) => {

  let swipeBtns = [{
    text: 'Delete',
    backgroundColor: 'red',
    underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
    onPress: () => { onRemove(contact.id) }
    // onPress: () => { this.deleteNote(rowData) }
  }];

  return (
    <View style={styles.contactWrapper}>
      <Swipeout right={swipeBtns}
        autoClose='true'
        backgroundColor='transparent'>
        <TouchableOpacity onPress={() => onOpen(contact, onRemove)} >
          <View style={styles.contactBlock}>
            <View style={styles.leftPart}>
              <FontAwesomeIcon style={styles.contactIcon} icon={faIdCardAlt} size={28} />
              <View>
                <Text style={styles.name}>{`${contact.name} ${contact.secondName}`}</Text>
                <Text>{contact.mail}</Text>
              </View>
            </View>
            <FontAwesomeIcon style={styles.infoIcon} icon={faInfoCircle} />
          </View>
        </TouchableOpacity>
      </Swipeout>
    </View>
  )
}

const styles = StyleSheet.create({
  contactWrapper: {
    marginBottom: 10,
  },
  contactBlock: {
    flexDirection: "row",
    alignItems: 'center',
    flex: 1,
    height: 60,
    backgroundColor: '#E3E6F2',

    width: '100%',
    justifyContent: 'space-between',
    paddingLeft: 30,
    paddingRight: 20,
    color: '#565656',
    borderRadius: 5
  },
  name: {
    color: '#565656',
    fontSize: 16,
    fontWeight: 'bold'
  },
  leftPart: {
    flexDirection: "row",
    alignItems: 'center'
  },
  contactIcon: {
    marginRight: 20,
    height: 20,
    color: '#565656'
  },
  infoIcon: {
    color: '#565656'
  }
})

export default Contact