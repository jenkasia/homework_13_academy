import React from 'react'
import { Linking, Text, TouchableHighlight, View, StyleSheet } from 'react-native'
import { faPhone, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { text } from '@fortawesome/fontawesome-svg-core'

const ButtonsBlock = ({ phoneNumber, onRemove, id }) => {
  return (
    <View style={styles.component}>
      <TouchableHighlight onPress={() => { Linking.openURL(`tel: ${phoneNumber}`) }}>
        <View style={styles.button}>
          <FontAwesomeIcon size={20} icon={faPhone} transform={{ rotate: 90 }} />
          <Text style={styles.text}>Call</Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight onPress={() => { onRemove(id) }}>
        <View style={styles.button}>
          <FontAwesomeIcon size={20} icon={faTimes} />
          <Text style={styles.text}>Delete</Text>
        </View>
      </TouchableHighlight>
    </View>

  )
}

const styles = StyleSheet.create({
  component: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: "space-between",
    marginTop: 20,
  },
  button: {
    backgroundColor: '#DADADA',
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 40,
    borderRadius: 20,
    fontSize: 10,
    alignItems: 'center'
  },
  text: {
    marginLeft: 10,
    fontSize: 20,
  }
})

export default ButtonsBlock