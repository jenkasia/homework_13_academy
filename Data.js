let data = [
  {
    id: '1',
    name: 'Ilon',
    secondName: 'Mask',
    phone: '+380662344302',
    mail: 'ilon.mask@gmail.com',
    avatar: 'https://upload.wikimedia.org/wikipedia/commons/4/49/Elon_Musk_2015.jpg'
  },
  {
    id: '2',
    name: 'Donald',
    secondName: 'Tramp',
    phone: '+380980032543',
    mail: 'donald.trump@gmail.com',
    avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Donald_Trump_official_portrait.jpg/250px-Donald_Trump_official_portrait.jpg'
  },
  {
    id: '3',
    name: 'Tim',
    secondName: 'Cook',
    phone: '+380664433959',
    mail: 'tim.cook@gmail.com',
    avatar: ''
  },
  {
    id: '4',
    name: 'Mark',
    secondName: 'Zuckerberg',
    phone: '+380662357834',
    mail: 'mark.zuckerberg@gmail.com',
    avatar: 'https://icdn.lenta.ru/images/2020/01/10/11/20200110115253541/pwa_vertical_1280_0b2197e8ea62b2062483aeeda6a324be.jpg'
  },
  {
    id: '5',
    name: 'Bill',
    secondName: 'Gates',
    phone: '+380662305932',
    mail: 'bill.gates@gmail.com',
    avatar: 'https://biz.liga.net/images/general/2019/07/17/thumbnail-tw-20190717155005-8015.jpg?v=1563373586'
  },
  {
    id: '6',
    name: 'Barack',
    secondName: 'Obama',
    phone: '+380662224622',
    mail: 'barack.obama@gmail.com',
    avatar: 'https://file.liga.net/images/general/2018/07/17/20180717202028-4108.jpg?v=1531854947'
  },
  {
    id: '7',
    name: 'Will',
    secondName: 'Smith',
    phone: '+380663642452',
    mail: 'will.smith@gmail.com',
    avatar: 'https://file.liga.net/images/general/2018/04/05/20180405145144-7279.jpg?v=1522936501'
  },
  {
    id: '8',
    name: 'Johnny',
    secondName: 'Depp',
    phone: '+380662033282',
    mail: 'johnny.depp@gmail.com',
    avatar: 'https://upload.wikimedia.org/wikipedia/commons/f/f9/JohnnyDeppByArnoldWells2011.jpg'
  },
  {
    id: '9',
    name: 'Michael',
    secondName: 'Jordan',
    phone: '+3802323232',
    mail: 'michael.jordan@gmail.com',
    avatar: 'https://s5o.ru/storage/simple/ru/edt/ad/15/7a/ec/rue0e718e4eff.jpg'
  },
  {
    id: '10',
    name: 'Tom',
    secondName: 'Cruise',
    phone: '+380664382354',
    mail: 'tom.cruise@gmail.com',
    avatar: 'https://s5.cdn.teleprogramma.pro/wp-content/uploads/2018/12/32819f7d7525e05e41fdd966630dfca7.jpg'
  },
  {
    id: '11',
    name: 'Dwayne',
    secondName: 'Johnson',
    phone: '+380664433002',
    mail: 'dwayne.johnson@gmail.com',
    avatar: 'https://s0.rbk.ru/v6_top_pics/ampresize/media/img/9/51/754721332085519.jpg'
  },
  {
    id: '12',
    name: 'Leonardo',
    secondName: 'DiCaprio',
    phone: '+380662222222',
    mail: 'leonardo.dicaprio@gmail.com',
    avatar: 'https://star.korupciya.com/wp-content/uploads/2016/01/hd_85f20787f9.jpg'
  },
]

export { data }