import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { MainScreen } from '../screens/MainScreen'
import { ContactScreen } from '../screens/ContactScreen'

const PostNavigator = createStackNavigator(
  {
    Main: MainScreen,
    Contact: {
      screen: ContactScreen
    }
  },
  {
    initialRouteName: 'Main'
  }
)

export const AppNavigation = createAppContainer(PostNavigator)
